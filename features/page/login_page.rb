#require File.expand_path('../page.rb', __FILE__)
require 'features/page/page'
class LoginPage < Page

  attr_reader :login_field, :password_field, :signin_button

  def initialize(session)
    super(session)
    raise "Invalid session" if session==nil
    @session=session
    @login_field = session.find(:id, "user_email")
    @password_field = session.find(:id, "user_password")
    @signin_button = session.find(:xpath, "//input[contains(@value,'Sign in')]")
  end

  def set_login(login)
    @login_field.send_keys(login)
    return self
  end

  def set_password(password)
    @password_field.send_keys(password)
    return self
  end

  def submit
    @signin_button.click
    return HomePage.new(@session)
  end

end