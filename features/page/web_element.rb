module WebElement

  class Menu
    def initialize(session)
      @session=session
    end

    def goto_home
      @session.find(:xpath,'//*[contains(@class,\'navbar-default\')]//descendant-or-self::*[contains(text(),\'Home\')]').click
      return HomePage.new(@session)
    end

    def goto_tasks
      @session.find(:xpath,'//*[contains(@class,\'navbar-default\')]//descendant-or-self::*[contains(text(),\'My Tasks\')]').click
      return CreateTaskPage.new(@session)
    end

    def goto_signin
      @session.click_link_or_button("Sign In")
      return LoginPage.new(@session)
    end

    def istasksvisible?
      return @session.find(:xpath,'//*[contains(@class,\'navbar-default\')]//descendant-or-self::*[contains(text(),\'My Tasks\')]').visible?
    end
  end

end