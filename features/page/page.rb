#require File.expand_path('../web_element.rb', __FILE__)
require 'features/page/web_element'

class Page
  include WebElement

  def initialize(session)
    @session=session
  end

  def menu
    return Menu.new(@session)
  end
end