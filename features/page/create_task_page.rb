require 'features/page/page'
class CreateTaskPage < Page


  attr_reader  :new_task, :add_task, :table_task, :title

  def initialize (session)
    super (session)
    @session = session
    @new_task = session.find(:id,"new_task")
    @add_task = session.find(:xpath,"//*[contains(@ng-click,'addTask()')]")
    @table_task=session.find(:xpath,"//*[contains(text(),'To be Done')]//following-sibling::table")
    @title=session.find(:xpath,"//h1[contains(text(),'ToDo List')]")
  end

  def set_task_name(value)
    @new_task.send_keys (value)
    self
  end

  def click_add
    @add_task.click
    self
  end



end