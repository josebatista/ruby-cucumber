require "bundler/setup"
require "capybara"
require "capybara/dsl"

$LOAD_PATH.unshift(File.expand_path("../../../../avenuecode-ruby", __FILE__))


Capybara.default_driver = :selenium_chrome
Capybara.default_wait_time = 20

module SessionFactory
  include Capybara::DSL

  def create_session
    return Capybara::Session.new(:selenium_chrome)
  end

end

World (SessionFactory)