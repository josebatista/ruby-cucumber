require 'yaml'


class Config

  attr_reader :url,:username,:password

  def initialize ()
    yaml = YAML.load_file(File.join(File.dirname(__FILE__), "../../resources/config.yaml"))
    @url = yaml['url']
    @username = yaml['username']
    @password = yaml['password']
  end

end

