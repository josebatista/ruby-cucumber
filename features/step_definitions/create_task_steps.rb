
Given (/^I am on the My Tasks page$/) do
  raise 'My Tasks page not loaded' if not @tasks_page.title.visible?
end



Then (/^I should be redirected to the page with all tasks created$/) do
  step "I am on the My Tasks page"
end


