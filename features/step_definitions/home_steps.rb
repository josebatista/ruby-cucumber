
Given (/^I can see 'My Tasks' link on the NavBar$/) do
  raise 'My Tasks is not visible on the NavBar' if not @home_page.menu.istasksvisible?
end

Given(/^I am logged as "([^"]*)" on ToDO App$/) do |name|
  pending # Write code here that turns the phrase above into concrete actions
end


When (/^I click in 'My Tasks' link on the NavBar$/) do
  @tasks_page = @home_page.menu.goto_tasks
end

Then(/^I should see the message "([^"]*)" on the top part$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end